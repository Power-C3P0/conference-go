from django.db import IntegrityError
from django.http import JsonResponse
from common.json import ModelEncoder
from events.models import Conference
from .models import Presentation, Status
from django.views.decorators.http import require_http_methods
import json


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
        "presenter_name",
    ]


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
        "presenter_name",
        "synopsis",
    ]


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    """
    Lists the presentation titles and the link to the
    presentation for the specified conference id.

    Returns a dictionary with a single key "presentations"
    which is a list of presentation titles and URLS. Each
    entry in the list is a dictionary that contains the
    title of the presentation, the name of its status, and
    the link to the presentation's information.

    {
        "presentations": [
            {
                "title": presentation's title,
                "status": presentation's status name
                "href": URL to the presentation,
            },
            ...
        ]
    }
    """
    """
    presentations = [
        {
            "title": p.title,
            "status": p.status.name,
            "href": p.get_api_url(),
        }
        for p in Presentation.objects.filter(conference=conference_id)
    ]

    return JsonResponse({"presentations": presentations})
    """
    if request.method == "GET":
        presentations = Presentation.objects.filter(conference=conference_id)
        return JsonResponse(
            {"presentations": presentations}, encoder=PresentationListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            presentation = Presentation.objects.create(**content)
            return JsonResponse(
                presentation, encoder=PresentationDetailEncoder, safe=False
            )
        except IntegrityError:
            content["conference"] = Conference.objects.get(id=conference_id)
            content["status"] = Status.objects.get(id=1)
            presentation = Presentation.objects.create(**content)
            return JsonResponse(
                presentation, encoder=PresentationDetailEncoder, safe=False
            )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, pk):
    """
    Returns the details for the Presentation model specified
    by the pk parameter.

    This should return a dictionary with the presenter's name,
    their company name, the presenter's email, the title of
    the presentation, the synopsis of the presentation, when
    the presentation record was created, its status name, and
    a dictionary that has the conference name and its URL

    {
        "presenter_name": the name of the presenter,
        "company_name": the name of the presenter's company,
        "presenter_email": the email address of the presenter,
        "title": the title of the presentation,
        "synopsis": the synopsis for the presentation,
        "created": the date/time when the record was created,
        "status": the name of the status for the presentation,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    """
    pres = Presentation.objects.get(id=pk)
    return JsonResponse(
        {
            "presenter_name": pres.presenter_name,
            "company_name": pres.company_name,
            "presenter_email": pres.presenter_email,
            "title": pres.title,
            "synopsis": pres.synopsis,
            "created": pres.created,
            "status": pres.status.name,
            "conference": {
                "name": pres.conference.name,
                "href": pres.conference.get_api_url(),
            },
        }
    )
    """
    if request.method == "GET":
        presentation = Presentation.objects.get(id=pk)
        return JsonResponse(
            presentation, encoder=PresentationDetailEncoder, safe=False
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        content["status"] = Status.objects.get(id=1)
        try:
            Presentation.objects.filter(id=pk).update(**content)
            presentation = Presentation.objects.get(id=pk)
            return JsonResponse(
                presentation,
                encoder=PresentationDetailEncoder,
                safe=False,
            )
        except ValueError:
            return JsonResponse("Not a valid conference input.")
    else:
        presentation = Presentation.objects.get(id=pk)
        name = presentation.title
        count, _ = Presentation.objects.filter(id=pk).delete()
        return JsonResponse(
            f"deleted: {count} presentation titled:so {name}", safe=False
        )
