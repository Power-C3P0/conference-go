from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder

# from events.models import Conference
from .models import Attendee, ConferenceVO
import json


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        "email",
        "company_name",
    ]


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """
    """
    attendees = Attendee.objects.filter(conference=conference_id)

    response = []
    for attendee in attendees:
        response.append(
            {
                "name": attendee.name,
                "href": attendee.get_api_url(),
                "conference": attendee.conference.name,
            }
        )
    return JsonResponse({"attendees": response})
    """
    # """
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees}, encoder=AttendeeListEncoder
        )
    # """
    else:
        content = json.loads(request.body)

        try:
            conference_href = content["conference"]
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except (ConferenceVO.DoesNotExist, KeyError):
            # content["conference"] = ConferenceVO.objects.get(
            #     id=conference_vo_id
            # )
            return JsonResponse(
                {
                    "message": "Invalid conference or conference field not added"
                },
                status=400,
            )
        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee, encoder=AttendeeDetailEncoder, safe=False
        )


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        "email",
        "company_name",
    ]


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_attendee(request, pk):
    """
    Returns the details for the Attendee model specified
    by the pk parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    """
    attendee = Attendee.objects.get(id=pk)
    return JsonResponse(
        {
            "email": attendee.email,
            "name": attendee.name,
            "company_name": attendee.company_name,
            "created": attendee.created,
            "conference": {
                "name": attendee.conference.name,
                "href": attendee.conference.get_api_url(),
            },
        }
    )
    """
    if request.method == "GET":
        attendee = Attendee.objects.get(id=pk)
        return JsonResponse(
            {"attendee": attendee}, encoder=AttendeeDetailEncoder
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        if "conference" in content:
            try:
                conference = ConferenceVO.objects.get(
                    name=content["conference"]
                )
                content["conference"] = conference
            except ConferenceVO.DoesNotExist:
                return JsonResponse(
                    {
                        "message": "Invalid conference name or conference does not exist"
                    }
                )
            Attendee.objects.filter(id=pk).update(**content)
            attendee = Attendee.objects.get(id=pk)
            return JsonResponse(
                attendee, encoder=AttendeeDetailEncoder, safe=False
            )
    else:
        try:
            attendee = Attendee.objects.get(id=pk)
            name = attendee.name
            count, _ = Attendee.objects.get(id=pk).delete()
            return JsonResponse(
                f"deleted: {count} attendee, {name}", safe=False
            )
        except Attendee.DoesNotExist:
            return JsonResponse(
                {
                    "message": "Attendee either not in the system or has been....erased..... from existence"
                }
            )
