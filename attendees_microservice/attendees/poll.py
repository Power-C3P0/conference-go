import json
import requests

from .models import ConferenceVO


def get_conference():
    url = "http://monolith:8000/api/conferences/"
    response = requests.get(url)
    content = json.loads(response.content)
    for con in content["conferences"]:
        ConferenceVO.objects.update_or_create(
            import_href=con["href"],
            defaults={"name": con["name"]},
        )
